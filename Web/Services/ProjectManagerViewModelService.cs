﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.ViewModels.User.ProjectManager;

namespace Web.Services
{
    public class ProjectManagerViewModelService
    {
        private readonly IProjectManagerRepository _projectManagerRepository;
        private readonly UserManager<IdentityUser> _userManager;

        public ProjectManagerViewModelService(IProjectManagerRepository projectManagerRepository, 
                                              UserManager<IdentityUser> userManager)
        {
            _projectManagerRepository = projectManagerRepository;
            _userManager = userManager;
        }

        public async Task<IEnumerable<ProjectManager>> GetIndexViewModel()
        {
            return await _projectManagerRepository.GetAsync(pm => pm.Projects);
        }


        public async Task<ProjectManagerDetailsViewModel> GetDetailsViewModel(int? id)
        {
            ProjectManager manager = (await _projectManagerRepository.GetAsync(pm => pm.Id == id, pm => pm.Projects)).FirstOrDefault(); 
            IdentityUser user = await _userManager.FindByIdAsync(manager?.IdentityUserId);
            ProjectManagerDetailsViewModel viewModel = null;

            if (manager != null && user != null)
            {
                viewModel = new ProjectManagerDetailsViewModel
                {
                    FullName = manager.FullName,
                    NumberOfProjects = manager.Projects.Count,
                    Email = user.Email,
                    Username = user.UserName
                };
            }
            return viewModel;
        }

        public async Task<ProjectManagerEditViewModel> GetEditViewModel(int? id)
        {
            ProjectManager manager = await _projectManagerRepository.GetByIdAsync(id);
            IdentityUser user = await _userManager.FindByIdAsync(manager?.IdentityUserId);
            ProjectManagerEditViewModel viewModel = null;

            if (manager != null && user != null)
            {
                viewModel = new ProjectManagerEditViewModel
                {
                    Id = manager.Id,
                    FirstName = manager.FirstName,
                    LastName = manager.LastName,
                    Email = user.Email,
                    UserName = user.UserName
                };
            }
            return viewModel;
        }

        public async Task<ProjectManagerDeleteViewModel> GetDeleteViewModel(int? id)
        {
            ProjectManager manager = (await _projectManagerRepository.GetAsync(pm => pm.Id == id, pm => pm.Projects)).FirstOrDefault();
            ProjectManagerDeleteViewModel viewModel = null;

            if (manager != null)
            {
                viewModel = new ProjectManagerDeleteViewModel
                {
                    Id = manager.Id,
                    Name = manager.LastName,
                    IdentityUserId = manager.IdentityUserId,
                    Projects = manager.Projects
                };
            }
            return viewModel;
        }
    }
}
