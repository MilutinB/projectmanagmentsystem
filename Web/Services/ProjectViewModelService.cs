﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.ViewModels.ProjectManagment.Project;
using ApplicationCore.Services;

namespace Web.Services
{
    public class ProjectViewModelService
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IProjectManagerRepository _managerRepository;
        private readonly ProjectService _projectService;
        public ProjectViewModelService(IProjectRepository projectRepository, 
                                       IProjectManagerRepository managerRepository,
                                       ProjectService projectService)
        {
            _projectRepository = projectRepository;
            _managerRepository = managerRepository;
            _projectService = projectService;
        }

        public async Task<IReadOnlyList<Project>> GetAllProjects()
        {
            IReadOnlyList<Project> projects = await _projectRepository.GetAsync(p => p.ProjectManager,
                x => x.ProjectManager.IdentityUser, p => p.Tasks);

            foreach (Project project in projects)
            {
                List<int> progressList = project.Tasks?.Select(t => t.Progress).ToList();
                project.Progress = CalculateProgress(progressList);
            }
            return projects;
        }

        public async Task<ProjectCreateViewModel> GetCreateViewModel(ProjectCreateViewModel viewModel = null)
        {
            IReadOnlyList<ProjectManager> managers = await _managerRepository.GetAllAsync();
            if (viewModel == null)
            {
                ProjectCreateViewModel newModel = new ProjectCreateViewModel
                {
                    ManagerId = 0,
                };
                newModel.Menagers = new SelectList(managers, "Id", "FullName", newModel.ManagerId);
                return newModel;
            }
            viewModel.Menagers = new SelectList(managers, "Id", "FullName", viewModel.ManagerId);
            return viewModel;
        }

        public async Task<ProjectDetailsViewModel> GetDetailsViewModel(int id, string filterTasks)
        {
            Project project = await _projectService.GetProject(id); 
            ProjectDetailsViewModel viewModel = null;

            if (project != null)
            {
                List<int> progressList = project.Tasks?.Select(t => t.Progress).ToList();

                viewModel = new ProjectDetailsViewModel
                {
                    Id = project.Id,
                    Code = project.Code,
                    Name = project.Name,
                    ProjectManager = project.ProjectManager,
                    Progress = CalculateProgress(progressList)
                };

                if (filterTasks != null)
                {
                    ProjectTaskStatus taskStatus = GetTaskStatus(filterTasks);
                    viewModel.FilterTasks = filterTasks;
                    viewModel.Tasks = project.Tasks?.Where(t => t.Status == taskStatus).ToList();
                }
            }
            return viewModel;
        }

        public async Task<ProjectEditViewModel> GetEditViewModel(int id)
        {
            Project project = await _projectService.GetProject(id);
            ProjectEditViewModel viewModel = null;

            if (project != null)
            {
                viewModel = new ProjectEditViewModel
                {
                    Id = project.Id,
                    Code = project.Code,
                    Name = project.Name,
                    Tasks = project.Tasks,
                    ProjectManagerId = project.ProjectManagerId
                };
                IReadOnlyList<ProjectManager> managers = await _managerRepository.GetAllAsync();
                viewModel.Menagers = new SelectList(managers, "Id", "FullName", viewModel.ProjectManagerId);
            }
            return viewModel;
        }

        public async Task<ProjectDeleteViewModel> GetDeleteViewModel(int id)
        {
            Project project = await _projectService.GetProject(id);
            ProjectDeleteViewModel viewModel = null;

            if (project != null)
            {
                List<int> progressList = project.Tasks?.Select(t => t.Progress).ToList();
                viewModel = new ProjectDeleteViewModel
                {
                    Id = project.Id,
                    Code = project.Code,
                    Name = project.Name,
                    Progress = CalculateProgress(progressList),
                    ProjectManagerName = project.ProjectManager.FullName
                };

                if (project.Tasks != null)
                {
                    viewModel.UnfinishedTasks = project.Tasks.Where(t => t.Progress != 100).Count();
                }
            }
            return viewModel;
        }

        private int CalculateProgress(List<int> progressList)
        {
            if (progressList.Count() == 0)
            {
                return 0;
            }
            double average = progressList.Aggregate((current, next) => current + next) / progressList.Count();
            return (int)average;
        }

        private ProjectTaskStatus GetTaskStatus(string filter)
        {
            ProjectTaskStatus taskStatus = ProjectTaskStatus.InProgress;
            switch (filter)
            {
                case "new":
                    taskStatus = ProjectTaskStatus.New;
                    break;
                case "inProgress":
                    taskStatus = ProjectTaskStatus.InProgress;
                    break;
                case "finished":
                    taskStatus = ProjectTaskStatus.Finished;
                    break;
                default:
                    break;
            }
            return taskStatus;
        }
    }
}

