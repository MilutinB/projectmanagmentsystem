﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.ViewModels.User.Developer;

namespace Web.Services
{
    public class DeveloperViewModelService
    {
        private readonly IDeveloperRepository _developerRepository;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ITaskRepository _taskRepository;

        public DeveloperViewModelService(IDeveloperRepository developerRepository, 
                                         UserManager<IdentityUser> userManager,
                                         ITaskRepository taskRepository)
        {
            _developerRepository = developerRepository;
            _userManager = userManager;
            _taskRepository = taskRepository;
        }

        public async Task<IEnumerable<Developer>> GetIndexViewModel()
        {
            return await _developerRepository.GetAllAsync();
        }

        public async Task<DeveloperDetailsViewModel> GetDetailsViewModel(int? id)
        {
            Developer developer = await _developerRepository.GetByIdAsync(id.Value);
            IdentityUser user = await _userManager.FindByIdAsync(developer?.IdentityUserId);
            DeveloperDetailsViewModel viewModel = null;

            if (developer != null)
            {
                viewModel = new DeveloperDetailsViewModel
                {
                    FullName = developer.FullName,
                    Email = user.Email,
                    NumberOfTasks = developer.TaskCount,
                    Username = user.UserName
                };
            }

            return viewModel;
        }

        public async Task<DeveloperEditViewModel> GetEditViewModel(int? id) 
        {
            Developer developer = await _developerRepository.GetByIdAsync(id);
            IdentityUser user = await _userManager.FindByIdAsync(developer?.IdentityUserId);
            DeveloperEditViewModel viewModel = null;

            if (developer != null)
            {
                viewModel = new DeveloperEditViewModel
                {
                    Id = developer.Id,
                    FirstName = developer.FirstName,
                    LastName = developer.LastName,
                    UserName = user.UserName,
                    Email = user.Email
                };
            }
            return viewModel;
        }

        public async Task<DeveloperDeleteViewModel> GetDeleteViewModel(int? id)
        {
            Developer developer = await _developerRepository.GetByIdAsync(id);
            DeveloperDeleteViewModel viewModel = null;

            if (developer != null)
            {
                viewModel = new DeveloperDeleteViewModel
                {
                    Id = developer.Id,
                    FullName = developer.FullName,
                    IdentityUserId = developer.IdentityUserId,
                    HasProjects = developer.TaskCount == 0 ? false : true 
                };
            }
            return viewModel;
        }

        public async Task<DeveloperTasksViewModel> GetTasksViewModel(int? id)
        {
            Developer developer = (await _developerRepository.GetAsync(d => d.Id == id, d => d.Tasks)).FirstOrDefault();
            IReadOnlyList<ProjectTask> tasks = await _taskRepository.GetAsync(
                t => t.DeveloperId == null && t.Progress == 0, t => t.Project);

            DeveloperTasksViewModel viewModel = null;

            if (developer != null)
            {
                if (developer.Tasks != null)
                {
                    developer.Tasks = developer.Tasks.Where(t => t.Progress != 100).ToList();
                }
                
                viewModel = new DeveloperTasksViewModel
                {
                    DeveloperTasks = developer.Tasks ?? null,
                    UnassignedTasks = tasks.ToList() ?? null
                };
            }
            return viewModel;
        }
    }
}
