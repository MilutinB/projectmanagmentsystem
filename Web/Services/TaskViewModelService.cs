﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.ViewModels.ProjectManagment.Task;

namespace Web.Services
{
    public class TaskViewModelService
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IDeveloperRepository _developerRepository;

        public TaskViewModelService(ITaskRepository taskRepository, IDeveloperRepository developerRepository)
        {
            _taskRepository = taskRepository;
            _developerRepository = developerRepository;
        }

        public async Task<TaskCreateViewModel> GetCreateViewModel(int projectId)
        {
            IReadOnlyCollection<Developer> developers = 
                await _developerRepository.GetAsync(d => d.TaskCount < 3, d => d.FullName, orderDescending: false);
            TaskCreateViewModel viewModel = new TaskCreateViewModel
            {
                ProjectId = projectId,
                Deadline = DateTime.Now.AddDays(1)
            };
            viewModel.Developers = new SelectList(developers, "Id", "FullName", viewModel.DeveloperId);
            return viewModel;
        }

        public async Task<TaskDetailsViewModel> GetDetailsViewModel(int id, int projectId)
        {
            ProjectTask task = (await _taskRepository.GetAsync(t => t.Id == id, t => t.Developer)).FirstOrDefault();
            TaskDetailsViewModel viewModel = null;

            if (task != null)
            {
                viewModel = new TaskDetailsViewModel
                {
                    Id = task.Id,
                    Name = task.Name,
                    Description = task.Description,
                    Deadline = task.Deadline,
                    DeveloperName = task.Developer?.FullName,
                    DeveloperId = task.DeveloperId ?? null,
                    Progress = task.Progress,
                    Status = task.Status,
                    ProjectId = projectId
                };
            }
            return viewModel;
        }

        public async Task<TaskEditViewModel> GetEditViewModel(int id)
        {
            List<Developer> developers = 
                (await _developerRepository.GetAsync(d => d.TaskCount < 3, d => d.FullName, orderDescending: false)).ToList();
            ProjectTask task = (await _taskRepository.GetAsync(t => t.Id == id, t => t.Developer)).FirstOrDefault();
            Developer developer = await _developerRepository.GetByIdAsync(task?.DeveloperId);
            TaskEditViewModel viewModel = null;

            if (task != null)
            {
                viewModel = new TaskEditViewModel
                {
                    Id = task.Id,
                    Name = task.Name,
                    Description = task.Description,
                    Deadline = task.Deadline,
                    DeveloperName = task.Developer?.LastName,
                    DeveloperId = task.DeveloperId,
                    Progress = task.Progress,
                    Status = task.Status,
                    ProjectId = task.ProjectId
                };

                if (developer != null && developer.TaskCount == 3)
                {
                    developers.Insert(0, developer);
                }

                viewModel.Developers = new SelectList(developers, "Id", "FullName", task.DeveloperId);
            }
            return viewModel;
        }

        public async Task<TaskDeleteViewModel> GetDeleteViewModel(int id)
        {
            ProjectTask task = await _taskRepository.GetByIdAsync(id);
            TaskDeleteViewModel viewModel = null;

            if (task != null)
            {
                viewModel = new TaskDeleteViewModel
                {
                    Id = task.Id,
                    Name = task.Name,
                    Description = task.Description,
                    Status = task.Status,
                    ProjectId = task.ProjectId
                };
                if (task.DeveloperId != null)
                {
                    viewModel.DeveloperId = task.DeveloperId;
                }
            }
            return viewModel;
        }
    }
}
