﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Services;
using Web.ViewModels.User.ProjectManager;

namespace Web.Controllers
{
    public class ProjectManagerController : Controller
    {
        private readonly IProjectManagerRepository _projectManagerRepository;
        private readonly ProjectManagerViewModelService _projectManagerViewModelService;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ProjectManagerService _projectManagerService;

        public ProjectManagerController(IProjectManagerRepository projectManagerRepository,
                                        ProjectManagerViewModelService projectManagerViewModelService,
                                        ProjectManagerService projectManagerService,
                                        UserManager<IdentityUser> userManager)
        {
            _projectManagerRepository = projectManagerRepository;
            _projectManagerViewModelService = projectManagerViewModelService;
            _userManager = userManager;
            _projectManagerService = projectManagerService;
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            return View(await _projectManagerViewModelService.GetIndexViewModel());
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View(new ProjectManagerCreateViewModel());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ProjectManagerCreateViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                IdentityResult result = null;

                try
                {
                    result = await _projectManagerService.Create(
                        viewModel.FirstName, viewModel.LastName, viewModel.UserName, viewModel.Email, viewModel.Password);
                }
                catch (AggregateException ex)
                {
                    ViewBag.ErrorTitle = "Error";
                    ViewBag.ErrorMessage = ex.Message;
                    return View("Error");
                }
                catch (DbUpdateException)
                {
                    ViewBag.ErrorTitle = "Error";
                    ViewBag.ErrorMessage = "Failed to create new project manager";
                    return View("Error");
                }

                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }

                foreach (IdentityError error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
            return View(viewModel);
        }

        [HttpGet]
        [Authorize(Roles = "Admin,Pm")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                ViewBag.ErrorMessage = $"Requested page could not be found";
                return View("NotFound");
            }

            if (User.IsInRole("Pm"))
            {
                if (!(await _projectManagerService.UserHasPermission(User, id)))
                {
                    return RedirectToAction("AccessDenied", "User");
                }
            }

            ProjectManagerDetailsViewModel viewModel = await _projectManagerViewModelService.GetDetailsViewModel(id.Value);

            if (viewModel == null)
            {
                ViewBag.ErrorMessage = $"Project manager with id '{id}' not found";
                return View("NotFound");
            }
            return View(viewModel);
        }

        [HttpGet]
        [Authorize(Roles = "Admin,Pm")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                ViewBag.ErrorMessage = $"Requested page could not be found";
                return View("NotFound");
            }

            if (User.IsInRole("Pm"))
            {
                if (!(await _projectManagerService.UserHasPermission(User, id)))
                {
                    return RedirectToAction("AccessDenied", "User");
                }
            }

            ProjectManagerEditViewModel viewModel = await _projectManagerViewModelService.GetEditViewModel(id.Value);

            if (viewModel == null)
            {
                ViewBag.ErrorMessage = $"Project manager with id '{id}' not found";
                return View("NotFound");
            }
            return View(viewModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ProjectManagerEditViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                ProjectManager manager = await _projectManagerRepository.GetByIdAsync(viewModel.Id);

                if (manager == null)
                {
                    ViewBag.ErrorMessage = $"Project manager with id '{viewModel.Id}' not found";
                    return View("NotFound");
                }

                IdentityResult result = null;

                try
                {
                    result = await _projectManagerService.Edit(User, viewModel.Id, viewModel.FirstName, viewModel.LastName, 
                        viewModel.UserName, viewModel.Email, viewModel.Password);
                }
                catch (AggregateException ex)
                {
                    ViewBag.ErrorTitle = "Error";
                    ViewBag.ErrorMessage = ex.Message;
                    return View("Error");
                }
                catch (DbUpdateException)
                {
                    ViewBag.ErrorTitle = "Error";
                    ViewBag.ErrorMessage = "Failed to edit project manager";
                    return View("Error");
                }

                if (result.Succeeded)
                {
                    return RedirectToAction("Details", new { Id = viewModel.Id });
                }
                foreach (IdentityError error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
            return View(viewModel);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                ViewBag.ErrorMessage = $"Requested page could not be found";
                return View("NotFound");
            }

            ProjectManagerDeleteViewModel viewModel = await _projectManagerViewModelService.GetDeleteViewModel(id);

            if (viewModel == null)
            {
                ViewBag.ErrorMessage = $"Project manager with id '{id}' not found";
                return View("NotFound");
            }
            return View(viewModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(ProjectManagerDeleteViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                ProjectManager manager = await _projectManagerRepository.GetByIdAsync(viewModel.Id);

                if (manager == null)
                {
                    ViewBag.ErrorMessage = $"Project manager with id '{viewModel.Id}' not found";
                    return View("NotFound");
                }

                IdentityResult result = null;

                try
                {
                    result = await _projectManagerService.Delete(manager.Id);
                }
                catch (AggregateException ex)
                {
                    ViewBag.ErrorTitle = "Error";
                    ViewBag.ErrorMessage = ex.Message;
                    return View("Error");
                }
                catch (DbUpdateException)
                {
                    ViewBag.ErrorTitle = "Error";
                    ViewBag.ErrorMessage = "Failed to delete project manager";
                    return View("Error");
                }

                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }

                foreach (IdentityError error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
            return View(viewModel);
        }
    }
}
