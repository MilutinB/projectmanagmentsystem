﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.ViewModels.User.Account;

namespace Web.Controllers
{
    [AllowAnonymous]
    public class UserController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly IProjectManagerRepository _projectManagerRepository;
        private readonly IDeveloperRepository _developerRepository;

        public UserController(UserManager<IdentityUser> userManager, 
                              SignInManager<IdentityUser> signInManager,
                              IProjectManagerRepository projectManagerRepository,
                              IDeveloperRepository developerRepository)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _projectManagerRepository = projectManagerRepository;
            _developerRepository = developerRepository;
        }

        [HttpGet]
        public IActionResult Login(string returnUrl)
        {
            if (_signInManager.IsSignedIn(User))
            {
                return RedirectToAction("Index", "Home");
            }
            LoginViewModel viewModel = new LoginViewModel { ReturnUrl = returnUrl };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                IdentityUser user = await _userManager.FindByNameAsync(viewModel.UserName);
                if (user == null)
                {
                    ModelState.AddModelError("", "Wrong user name or password!");
                    return View(viewModel);
                }

                Microsoft.AspNetCore.Identity.SignInResult result =
                    await _signInManager.PasswordSignInAsync(user, viewModel.Password, viewModel.RememberMe, false);
                if (result.Succeeded && viewModel.ReturnUrl == null)
                {
                    return RedirectToAction("Index", "Home");
                }
                else if (result.Succeeded && viewModel.ReturnUrl != null)
                {
                    return LocalRedirect(viewModel.ReturnUrl);
                }
                ModelState.AddModelError("", "Wrong user name or password!");
            }
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Login", "User");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult AccessDenied()
        {
            return View();
        }

        [AcceptVerbs("Get", "Post")]
        [AllowAnonymous]
        public async Task<IActionResult> IsPmUsernameTaken(string userName, int id)
        {
            IdentityUser user = await _userManager.FindByNameAsync(userName);

            if (user == null)
            {
                return Json(true);
            }
            else
            {
                if (User.IsInRole("Admin"))
                {
                    ProjectManager manager = (await _projectManagerRepository.GetAsync(pm => pm.Id == id, pm => pm.IdentityUser))
                        .FirstOrDefault();

                    if (manager?.IdentityUser.UserName == userName)
                    {
                        return Json(true);
                    }
                    return Json($"User name '{userName}' is already taken.");
                }
                else
                {
                    string nameToCompare = _userManager.GetUserName(User);

                    if (nameToCompare == userName)
                    {
                        return Json(true);
                    }
                    return Json($"User name '{userName}' is already taken.");
                }
            }
        }

        [AcceptVerbs("Get", "Post")]
        [AllowAnonymous]
        public async Task<IActionResult> IsDevUsernameTaken(string userName, int id)
        {
            IdentityUser user = await _userManager.FindByNameAsync(userName);

            if (user == null)
            {
                return Json(true);
            }
            else
            {
                if (User.IsInRole("Admin"))
                {
                    Developer developer = (await _developerRepository.GetAsync(d => d.Id == id, d => d.IdentityUser)).FirstOrDefault();

                    if (developer?.IdentityUser.UserName == userName)
                    {
                        return Json(true);
                    }
                    return Json($"User name '{userName}' is already taken.");
                }
                else
                {
                    string nameToCompare = _userManager.GetUserName(User);

                    if (nameToCompare == userName)
                    {
                        return Json(true);
                    }
                    return Json($"User name '{userName}' is already taken.");
                }
            }
        }
    }
}
