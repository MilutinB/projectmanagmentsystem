﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Services;
using Web.ViewModels.User.Developer;

namespace Web.Controllers
{
    public class DeveloperController : Controller
    {
        private readonly DeveloperViewModelService _developerViewModelService;
        private readonly DeveloperService _developerService;
        private readonly IDeveloperRepository _developerRepository;
        private readonly UserManager<IdentityUser> _userManager;

        public DeveloperController(DeveloperViewModelService developerViewModelService,
                                   DeveloperService developerService,
                                   IDeveloperRepository developerRepository,
                                   UserManager<IdentityUser> userManager)
        {
            _developerViewModelService = developerViewModelService;
            _developerService = developerService;
            _developerRepository = developerRepository;
            _userManager = userManager;
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            return View(await _developerViewModelService.GetIndexViewModel());
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View(new DeveloperCreateViewModel());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(DeveloperCreateViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                IdentityResult result = null;

                try
                {
                    result = await _developerService.Create(
                        viewModel.FirstName, viewModel.LastName, viewModel.UserName, viewModel.Email, viewModel.Password);
                }
                catch (AggregateException ex)
                {
                    ViewBag.ErrorTitle = "Error";
                    ViewBag.ErrorMessage = ex.Message;
                    return View("Error");
                }
                catch (DbUpdateException)
                {
                    ViewBag.ErrorTitle = "Error";
                    ViewBag.ErrorMessage = "Failed to create new developer";
                    return View("Error");
                }

                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }

                foreach (IdentityError error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
            return View(viewModel);
        }

        [HttpGet]
        [Authorize(Roles = "Admin,Dev")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                ViewBag.ErrorMessage = $"Requested page could not be found";
                return View("NotFound");
            }

            if (User.IsInRole("Dev"))
            {
                if (!(await _developerService.UserHasPermission(User, id)))
                {
                    return RedirectToAction("AccessDenied", "User");
                }
            }

            DeveloperDetailsViewModel viewModel = await _developerViewModelService.GetDetailsViewModel(id.Value);

            if (viewModel == null)
            {
                ViewBag.ErrorMessage = $"Developer with id '{id}' not found";
                return View("NotFound");
            }
            return View(viewModel);
        }

        [HttpGet]
        [Authorize(Roles = "Admin,Dev")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                ViewBag.ErrorMessage = $"Requested page could not be found";
                return View("NotFound");
            }

            if (User.IsInRole("Dev"))
            {
                if (!(await _developerService.UserHasPermission(User, id)))
                {
                    return RedirectToAction("AccessDenied", "User");
                }
            }

            DeveloperEditViewModel viewModel = await _developerViewModelService.GetEditViewModel(id.Value);

            if (viewModel == null)
            {
                ViewBag.ErrorMessage = $"Developer with id '{id}' not found";
                return View("NotFound");
            }
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(DeveloperEditViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                Developer developer = await _developerRepository.GetByIdAsync(viewModel.Id);

                if (developer == null)
                {
                    ViewBag.ErrorMessage = $"Developer with id '{viewModel.Id}' not found";
                    return View("NotFound");
                }

                IdentityResult result = null;

                try
                {
                    result = await _developerService.Edit(User, viewModel.Id, viewModel.FirstName, viewModel.LastName,
                        viewModel.UserName, viewModel.Email, viewModel.Password);
                }
                catch (AggregateException ex)
                {
                    ViewBag.ErrorTitle = "Error";
                    ViewBag.ErrorMessage = ex.Message;
                    return View("Error");
                }
                catch (DbUpdateException)
                {
                    ViewBag.ErrorTitle = "Error";
                    ViewBag.ErrorMessage = "Failed to edit developer";
                    return View("Error");
                }

                if (result.Succeeded)
                {
                    return RedirectToAction("Details", new { Id = viewModel.Id });
                }
                foreach (IdentityError error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
            return View(viewModel);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                ViewBag.ErrorMessage = $"Requested page could not be found";
                return View("NotFound");
            }

            DeveloperDeleteViewModel viewModel = await _developerViewModelService.GetDeleteViewModel(id.Value);

            if (viewModel == null)
            {
                ViewBag.ErrorMessage = $"Developer with id '{id}' not found";
                return View("NotFound");
            }
            return View(viewModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(DeveloperDeleteViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                Developer developer = await _developerRepository.GetByIdAsync(viewModel.Id);

                if (developer == null)
                {
                    ViewBag.ErrorMessage = $"Developer with id '{viewModel.Id}' not found";
                    return View("NotFound");
                }

                IdentityResult result = null;

                try
                {
                    result = await _developerService.Delete(developer.Id);
                }
                catch (AggregateException ex)
                {
                    ViewBag.ErrorTitle = "Error";
                    ViewBag.ErrorMessage = ex.Message;
                    return View("Error");
                }
                catch (DbUpdateException)
                {
                    ViewBag.ErrorTitle = "Error";
                    ViewBag.ErrorMessage = "Failed to delete developer";
                    return View("Error");
                }

                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }

                foreach (IdentityError error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
            return View(viewModel);
        }

        public async Task<IActionResult> Tasks(int? id)
        {
            if (id == null)
            {
                ViewBag.ErrorMessage = $"Requested page could not be found";
                return View("NotFound");
            }

            DeveloperTasksViewModel viewModel = await _developerViewModelService.GetTasksViewModel(id.Value);

            if (viewModel == null)
            {
                ViewBag.ErrorMessage = $"Developer with id '{id}' not found";
                return View("NotFound");
            }
            return View(viewModel);
        }
    }
}
