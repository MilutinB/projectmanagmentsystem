﻿using ApplicationCore.Services;
using ApplicationCore.Interfaces;
using ApplicationCore.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Services;
using Web.ViewModels.ProjectManagment.Task;

namespace Web.Controllers
{
    public class TaskController : Controller
    {
        private readonly TaskViewModelService _taskViewModelService;
        private readonly TaskService _taskService;
        private readonly DeveloperService _developerService;
        private readonly ITaskRepository _taskRepository;

        public TaskController(TaskViewModelService taskViewModelService,
                              TaskService taskService,
                              DeveloperService developerService,
                              ITaskRepository taskRepository)
        {
            _taskViewModelService = taskViewModelService;
            _taskService = taskService;
            _developerService = developerService;
            _taskRepository = taskRepository;
        }

        [HttpGet]
        [Authorize(Roles = "Admin,Pm")]
        public async Task<IActionResult> Create(int projectId)
        {
            TaskCreateViewModel viewModel = await _taskViewModelService.GetCreateViewModel(projectId);
            return View(viewModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(TaskCreateViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _taskService.Create(
                    viewModel.Name, viewModel.Description, viewModel.Deadline, viewModel.ProjectId, viewModel.DeveloperId);
                }
                catch (DbUpdateException)
                {
                    ViewBag.ErrorTitle = "Error";
                    ViewBag.ErrorMessage = "Failed to create new task";
                    return View("Error");
                }

                if (viewModel.DeveloperId != null)
                {
                    await _developerService.IncreaseTaskCount(viewModel.DeveloperId.Value);
                }

                return RedirectToAction("Details", "Project", new { Id = viewModel.ProjectId });
            }
            return View(_taskViewModelService.GetCreateViewModel(viewModel.ProjectId));
        }

        [HttpGet]
        [Authorize(Roles = "Admin,Pm,Dev")]
        public async Task<IActionResult> Details(int? id, int projectId)
        {
            if (id == null)
            {
                ViewBag.ErrorMessage = $"Requested page could not be found";
                return View("NotFound");
            }

            if (User.IsInRole("Dev"))
            {
                if (!(await _taskService.UserHasPermission(User, id)))
                {
                    return RedirectToAction("AccessDenied", "User");
                }
            }

            TaskDetailsViewModel viewModel = await _taskViewModelService.GetDetailsViewModel(id.Value, projectId);

            if (viewModel == null)
            {
                ViewBag.ErrorMessage = $"Task with id '{id}' not found";
                return View("NotFound");
            }
            return View(viewModel);
        }

        [HttpGet]
        [Authorize(Roles = "Admin,Pm,Dev")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                ViewBag.ErrorMessage = $"Requested page could not be found";
                return View("NotFound");
            }

            ProjectTask task = await _taskRepository.GetByIdAsync(id);

            if (task != null && task.Progress == 100)
            {
                ViewBag.ErrorTitle = "Error";
                ViewBag.ErrorMessage = "This tasked is marked as finished and can't be edited anymore";
                return View("Error");
            }

            if (User.IsInRole("Dev"))
            {
                if (!(await _taskService.UserHasPermission(User, id)))
                {
                    return RedirectToAction("AccessDenied", "User");
                }
            }

            TaskEditViewModel viewModel = await _taskViewModelService.GetEditViewModel(id.Value);

            if (viewModel == null)
            {
                ViewBag.ErrorMessage = $"Task with id '{id}' not found";
                return View("NotFound");
            }
            return View(viewModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(TaskEditViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _taskService.Edit(viewModel.Id, viewModel.Name, viewModel.Description,
                        viewModel.Deadline, viewModel.Progress, viewModel.DeveloperId, viewModel.ProjectId);
                }
                catch (DbUpdateException)
                {
                    ViewBag.ErrorTitle = "Error";
                    ViewBag.ErrorMessage = "Failed to create new task";
                    return View("Error");
                }

                return RedirectToAction("Details", "Task", new { Id = viewModel.Id, ProjectId = viewModel.ProjectId });
            }
            return View(viewModel);
        }

        [HttpGet]
        [Authorize(Roles = "Admin,Pm")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                ViewBag.ErrorMessage = $"Requested page could not be found";
                return View("NotFound");
            }

            TaskDeleteViewModel viewModel = await _taskViewModelService.GetDeleteViewModel(id.Value);

            if (viewModel == null)
            {
                ViewBag.ErrorMessage = $"Task with id '{id}' not found";
                return View("NotFound");
            }
            return View(viewModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(TaskDeleteViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _taskService.Delete(viewModel.Id, viewModel.DeveloperId);
                }
                catch (DbUpdateException)
                {
                    ViewBag.ErrorTitle = "Error";
                    ViewBag.ErrorMessage = "Failed to create new task";
                    return View("Error");
                }
                
                return RedirectToAction("Details", "Project", new { Id = viewModel.ProjectId });
            }
            return View(viewModel);
        }
    }
}
