﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Services;
using Web.ViewModels.ProjectManagment.Project;

namespace Web.Controllers
{
    [Authorize(Roles="Admin,Pm")]
    public class ProjectController : Controller
    {
        private readonly ProjectService _projectService;
        private readonly ProjectViewModelService _projectViewModelService;
        public ProjectController(ProjectService projectService, ProjectViewModelService projectViewModelService)
        {
            _projectService = projectService;
            _projectViewModelService = projectViewModelService;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View(await _projectViewModelService.GetAllProjects());
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            return View(await _projectViewModelService.GetCreateViewModel());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ProjectCreateViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _projectService.Create(viewModel.Name, viewModel.ManagerId);
                }
                catch (DbUpdateException)
                {
                    ViewBag.ErrorTitle = "Error";
                    ViewBag.ErrorMessage = "Failed to create new project";
                    return View("Error");
                }
                
                return RedirectToAction("Index");
            }
            return View(_projectViewModelService.GetCreateViewModel(viewModel));
        }

        [HttpGet]
        public async Task<IActionResult> Details(int? id, string filterTasks)
        {
            if (id == null)
            {
                ViewBag.ErrorMessage = $"Requested page could not be found";
                return View("NotFound");
            }

            ProjectDetailsViewModel viewModel = await _projectViewModelService.GetDetailsViewModel(id.Value, filterTasks);

            if (viewModel == null)
            {
                ViewBag.ErrorMessage = $"Project with id '{id}' not found";
                return View("NotFound");
            }
            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                ViewBag.ErrorMessage = $"Requested page could not be found";
                return View("NotFound");
            }

            ProjectEditViewModel viewModel = await _projectViewModelService.GetEditViewModel(id.Value);

            if (viewModel == null)
            {
                ViewBag.ErrorMessage = $"Project with id '{id}' not found";
                return View("NotFound");
            }
            return View(viewModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ProjectEditViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _projectService.Edit(viewModel.Id, viewModel.Name, viewModel.ProjectManagerId);
                }
                catch (DbUpdateException)
                {
                    ViewBag.ErrorTitle = "Error";
                    ViewBag.ErrorMessage = "Failed to edit project";
                    return View("Error");
                }

                return RedirectToAction("Index");
            }
            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                ViewBag.ErrorMessage = $"Requested page could not be found";
                return View("NotFound");
            }

            ProjectDeleteViewModel viewModel = await _projectViewModelService.GetDeleteViewModel(id.Value);

            if (viewModel == null)
            {
                ViewBag.ErrorMessage = $"Project with id '{id}' not found";
                return View("NotFound");
            }
            return View(viewModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(ProjectDeleteViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _projectService.Delete(viewModel.Id);
                }
                catch (DbUpdateException)
                {
                    ViewBag.ErrorTitle = "Error";
                    ViewBag.ErrorMessage = "Failed to delete project";
                    return View("Error");
                }
                return RedirectToAction("Index");
            }
            return View(viewModel);
        }
    }
}
