﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels.User.Developer
{
    public class DeveloperDeleteViewModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string IdentityUserId { get; set; }
        public bool HasProjects { get; set; }
    }
}
