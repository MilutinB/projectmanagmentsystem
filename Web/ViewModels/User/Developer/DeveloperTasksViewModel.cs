﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Entities;

namespace Web.ViewModels.User.Developer
{
    public class DeveloperTasksViewModel
    {
        public ICollection<ProjectTask> DeveloperTasks { get; set; }
        public ICollection<ProjectTask> UnassignedTasks { get; set; }
    }
}
