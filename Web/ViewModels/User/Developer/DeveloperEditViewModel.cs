﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels.User.Developer
{
    public class DeveloperEditViewModel
    {
        public int Id { get; set; }

        [Display(Name = "First name")]
        [Required(ErrorMessage = "Please enter developer's first name.")]
        [MaxLength(50, ErrorMessage = "Name is too long. Must be under 50 characters.")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        [Required(ErrorMessage = "Please enter developer's last name.")]
        [MaxLength(50, ErrorMessage = "Name is too long. Must be under 50 characters.")]
        public string LastName { get; set; }

        [Display(Name = "e-Mail")]
        [Required(ErrorMessage = "Please enter developer's e-mail address.")]
        [RegularExpression(@"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
            ErrorMessage = "Invalid e-Mail address.")]
        public string Email { get; set; }

        [Display(Name = "User name")]
        [Required(ErrorMessage = "Please enter developer's user name.")]
        [MaxLength(50, ErrorMessage = "Name is too long. Must be under 50 characters.")]
        [Remote("IsDevUsernameTaken", "User", AdditionalFields = "Id")]
        public string UserName { get; set; }

        [Display(Name = "New password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Confirm password")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Confirmation does not match.")]
        public string ConfirmPassword { get; set; }
    }
}
