﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels.User.ProjectManager
{
    public class ProjectManagerDetailsViewModel
    {
        [Display(Name = "Name")]
        public string FullName { get; set; }

        [Display(Name = "User name")]
        public string Username { get; set; }

        [Display(Name = "eMail")]
        public string Email { get; set; }

        [Display(Name = "Number of projects")]
        public int NumberOfProjects { get; set; }
    }
}
