﻿using ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels.User.ProjectManager
{
    public class ProjectManagerDeleteViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string IdentityUserId { get; set; }
        public ICollection<Project> Projects { get; set; }
    }
}
