﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels.User.ProjectManager
{
    public class ProjectManagerCreateViewModel
    {
        [Display(Name = "First name")]
        [Required(ErrorMessage = "Please enter manager's first name.")]
        [MaxLength(50, ErrorMessage = "Name is too long. Must be under 50 characters.")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        [Required(ErrorMessage = "Please enter manager's last name.")]
        [MaxLength(50, ErrorMessage = "Name is too long. Must be under 50 characters.")]
        public string LastName { get; set; }

        [Display(Name = "e-Mail")]
        [Required(ErrorMessage = "Please enter manager's e-mail address.")]
        [RegularExpression(@"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
            ErrorMessage = "Invalid e-Mail address.")]
        public string Email { get; set; }

        [Display(Name = "User name")]
        [Required(ErrorMessage = "Please enter manager's user name.")]
        [MaxLength(50, ErrorMessage = "Name is too long. Must be under 50 characters.")]
        [Remote("IsPmUsernameTaken", "User")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter manager's password.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Confirm password")]
        [Required(ErrorMessage = "Please enter manager's password.")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Confirmation does not match.")]
        public string ConfirmPassword { get; set; }
    }
}
