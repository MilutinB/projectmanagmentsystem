﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels.User.Account
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "You forgot to type in your user name.")]
        [Display(Name="User Name")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "No password provided.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }
    }
}
