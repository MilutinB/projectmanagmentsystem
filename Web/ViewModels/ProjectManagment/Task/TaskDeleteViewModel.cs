﻿using ApplicationCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels.ProjectManagment.Task
{
    public class TaskDeleteViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ProjectTaskStatus Status { get; set; }
        public int ProjectId { get; set; }
        public int? DeveloperId { get; set; }
    }
}
