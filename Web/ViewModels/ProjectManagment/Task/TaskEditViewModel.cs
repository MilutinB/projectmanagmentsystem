﻿using ApplicationCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels.ProjectManagment.Task
{
    public class TaskEditViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Task must have a name.")]
        [MaxLength(50, ErrorMessage = "Task name must be under 50 characters long.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Task must have a decription.")]
        [MaxLength(300, ErrorMessage = "Task description must be under 300 characters long.")]
        [MinLength(10, ErrorMessage = "Task description must at least 10 characters.")]
        public string Description { get; set; }

        public int Progress { get; set; }

        public ProjectTaskStatus Status { get; set; }

        [Required(ErrorMessage = "Task must have a deadline.")]
        [DataType(DataType.Date)]
        public DateTime Deadline { get; set; }

        public int ProjectId { get; set; }

        public int? DeveloperId { get; set; }

        [Display(Name = "Developer assigned.")]
        public string DeveloperName { get; set; }

        [Display(Name = "Change developer")]
        public SelectList Developers { get; set; }
    }
}
