﻿using ApplicationCore;
using ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels.ProjectManagment.Task
{
    public class TaskDetailsViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Progress { get; set; }
        public ProjectTaskStatus Status { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Deadline { get; set; }
        public int ProjectId { get; set; }
        public int? DeveloperId { get; set; }

        [Display(Name = "Developer assigned")]
        public string DeveloperName { get; set; }
    }
}
