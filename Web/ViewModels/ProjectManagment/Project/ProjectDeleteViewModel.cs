﻿using ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels.ProjectManagment.Project
{
    public class ProjectDeleteViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Progress { get; set; }
        public int? UnfinishedTasks { get; set; }

        [Display(Name = "Project Manager")]
        public string ProjectManagerName { get; set; }
    }
}
