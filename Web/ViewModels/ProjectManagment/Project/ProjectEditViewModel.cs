﻿using ApplicationCore.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Web.ViewModels.ProjectManagment.Project
{
    public class ProjectEditViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        [Required(ErrorMessage = "Please enter project name.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please assign a production menager.")]
        public int ProjectManagerId { get; set; }

        [Display(Name = "Production Manager")]
        public SelectList Menagers { get; set; }
        public ICollection<ProjectTask> Tasks { get; set; }
    }
}
