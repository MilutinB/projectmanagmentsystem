﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels.ProjectManagment.Project
{
    public class ProjectCreateViewModel
    {
        [Required(ErrorMessage="Please enter project name.")]
        public string Name { get; set; }

        [Display(Name="Production Manager")]
        public SelectList Menagers { get; set; }

        [Required(ErrorMessage="Please assign a production menager.")]
        public int ManagerId { get; set; }
    }
}
