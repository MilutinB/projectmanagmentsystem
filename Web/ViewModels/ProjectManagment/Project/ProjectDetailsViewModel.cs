﻿using ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Web.ViewModels.ProjectManagment.Project
{
    public class ProjectDetailsViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        [Display(Name = "Progress")]
        public int Progress { get; set; }
        public int ProjectManagerId { get; set; }
        public string FilterTasks { get; set; }

        [Display(Name = "Project Manager")]
        public ProjectManager ProjectManager { get; set; }
        public ICollection<ProjectTask> Tasks { get; set; }
    }
}
