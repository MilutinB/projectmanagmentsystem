﻿using System;
using System.Collections.Generic;
using ApplicationCore.Interfaces;
using ApplicationCore.Entities;

namespace DataAccess.Repositories
{
    public class AdministratorRepository : Repository<Administrator>, IAdministratorRepository
    {
        public AdministratorRepository(AppDbContext context) : base(context)
        {
        }
    }
}
