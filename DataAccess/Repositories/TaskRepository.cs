﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Repositories
{
    public class TaskRepository : Repository<ProjectTask>, ITaskRepository
    {
        public TaskRepository(AppDbContext context) : base(context)
        {

        }
    }
}
