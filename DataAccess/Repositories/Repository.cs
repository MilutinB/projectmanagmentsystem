﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories
{
    public class Repository<TEntity> : IRepositoryAsync<TEntity> where TEntity : class
    {
        protected readonly AppDbContext _context;

        public Repository(AppDbContext context)
        {
            _context = context;
        }

        public async Task AddAsync(TEntity entity)
        {
            await _context.Set<TEntity>().AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<IReadOnlyList<TEntity>> GetAllAsync()
        {
            return await _context.Set<TEntity>().ToListAsync();
        }

        public async Task<TEntity> GetByIdAsync(object id)
        {
            return await _context.Set<TEntity>().FindAsync(id);
        }

        public async Task<IReadOnlyList<TEntity>> GetAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await _context.Set<TEntity>().Where(predicate).ToListAsync();
        }

        public async Task<IReadOnlyList<TEntity>> GetAsync(params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            return await AddIncludesToQuery(_context.Set<TEntity>(), includeExpressions).ToListAsync();
        }

        public async Task<IReadOnlyList<TEntity>> GetAsync(Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            return await AddIncludesToQuery(_context.Set<TEntity>(), includeExpressions).Where(predicate).ToListAsync();
        }

        public async Task<IReadOnlyList<TEntity>> GetAsync(Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, object>> orderBy, bool orderDescending)
        {
            if (orderDescending)
            {
                return await _context.Set<TEntity>().Where(predicate).OrderByDescending(orderBy).ToListAsync();
            }
            else
            {
                return await _context.Set<TEntity>().Where(predicate).OrderBy(orderBy).ToListAsync();
            }
        }

        public async Task<IReadOnlyList<TEntity>> GetAsync(Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, object>> orderBy,
            bool orderDescending,
            params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            if (orderDescending)
            {
                return await AddIncludesToQuery(_context.Set<TEntity>(), includeExpressions)
                    .Where(predicate)
                    .OrderByDescending(orderBy)
                    .ToListAsync();
            }
            else
            {
                return await AddIncludesToQuery(_context.Set<TEntity>(), includeExpressions)
                    .Where(predicate)
                    .OrderBy(orderBy)
                    .ToListAsync();
            }
        }

        public async Task UpdateAsync(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(TEntity entity)
        {
            _context.Set<TEntity>().Remove(entity);
            await _context.SaveChangesAsync();
        }

        protected IQueryable<TEntity> AddIncludesToQuery(IQueryable<TEntity> dbSet, 
            params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            return includeExpressions.Aggregate(dbSet, (current, next) => current.Include(next));
        }
    }
}
