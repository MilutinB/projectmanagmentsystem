﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Repositories
{
    public class ProjectManagerRepository : Repository<ProjectManager>, IProjectManagerRepository
    {
        public ProjectManagerRepository(AppDbContext context) : base(context)
        {
        }
    }
}
