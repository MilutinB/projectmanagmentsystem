﻿using ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using ApplicationCore.Interfaces;

namespace DataAccess.Repositories
{
    public class DeveloperRepository : Repository<Developer>, IDeveloperRepository
    {
        public DeveloperRepository(AppDbContext context) : base(context)
        {

        }
    }
}
