﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Administrator",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Role = table.Column<int>(nullable: false),
                    IdentityUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Administrator", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Administrator_AspNetUsers_IdentityUserId",
                        column: x => x.IdentityUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Developer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Role = table.Column<int>(nullable: false),
                    TaskCount = table.Column<int>(nullable: false),
                    IdentityUserId = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Developer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Developer_AspNetUsers_IdentityUserId",
                        column: x => x.IdentityUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProjectManager",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Role = table.Column<int>(nullable: false),
                    IdentityUserId = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectManager", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectManager_AspNetUsers_IdentityUserId",
                        column: x => x.IdentityUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Project",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Progress = table.Column<int>(nullable: false),
                    ProjectManagerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Project_ProjectManager_ProjectManagerId",
                        column: x => x.ProjectManagerId,
                        principalTable: "ProjectManager",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Task",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Progress = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    Deadline = table.Column<DateTime>(nullable: false),
                    ProjectId = table.Column<int>(nullable: false),
                    DeveloperId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Task", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Task_Developer_DeveloperId",
                        column: x => x.DeveloperId,
                        principalTable: "Developer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Task_Project_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Project",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "22926528-aff3-45ed-a837-eb4cd431319c", "42eea48c-61ac-431c-b8b8-b1da0b6a645c", "Admin", "ADMIN" },
                    { "62965855-2391-47f7-aff6-6741d8353f1b", "f52eb284-eab5-4b78-bd09-99495af93683", "Pm", "PM" },
                    { "f5041f31-ac79-4236-8b57-2d8955fa8a39", "de9bc41e-cf77-49f8-99b1-7eab59017ab6", "Dev", "DEV" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { "e7b5286f-3bd1-497a-89c4-1cf5b83081d5", 0, "992f964e-ad26-4dcf-9522-1904cff2cadd", "admin@company.com", false, false, null, "ADMIN@COMPANY.COM", "ADMIN", "AQAAAAEAACcQAAAAEHb05EHr22VMRa2aQBEPJo6sE9cbdBRHZTyj3G0R0MxpURajouJcPVgOBUcc9wYUVQ==", null, false, "9c1413af-ce25-4f86-b1d0-c5d3a9a3ac15", false, "Admin" },
                    { "e12ee330-d7b0-4dfb-b2ef-604d434233f1", 0, "464e7694-1a95-4321-b76f-5c3e26441329", "pm01@company.com", false, false, null, "PM01@COMPANY.COM", "PM01", "AQAAAAEAACcQAAAAENGocv1UZ8nMWFG8s/Lu3uWTuJlWOfNNffaCPxBdzWDpoWIq1A3IP4GJiX+ssYbM1w==", null, false, "1c3b1b1f-880a-45f6-a02c-a378f959f418", false, "Pm01" },
                    { "4adfbd8d-4e69-45b2-8323-9b407bf000b3", 0, "6b1ce1e6-0c1c-4f39-83d6-896c92370803", "pm02@company.com", false, false, null, "PM02@COMPANY.COM", "PM02", "AQAAAAEAACcQAAAAEI58F/SmqE+iAM9vFc5GV4QWeZyNYpKJDSIECAQlDnke5zEcy0/vEDsgwVELAN0fIg==", null, false, "1e937efe-fb7e-43a8-81b1-661f742b1b06", false, "Pm02" },
                    { "dc0a641a-8bfc-4c7e-aff2-578a139752ef", 0, "03f8bcb9-488a-4ddf-b758-30409421bddd", "dev01@company.com", false, false, null, "DEV01@COMPANY.COM", "DEV01", "AQAAAAEAACcQAAAAELS0+mt0DEtt5yrzeEEwMF8q4sla1tGujCzYMA7ISxrym0bUw15cwIwMb7r3IxX7lw==", null, false, "bfcfe600-6b9e-4ab6-ba9d-65f3fa5b5e0d", false, "Dev01" },
                    { "85179301-cebb-49c1-8753-e5e5507823fc", 0, "a5f21e94-91fc-4960-bd5c-e5fac913059e", "dev02@company.com", false, false, null, "DEV02@COMPANY.COM", "DEV02", "AQAAAAEAACcQAAAAEE+Aa8zjHmWNMchh9LKE2aGLPGqPaj9WD9fvSa6HsHc7W0zeaoWpgDcwrUIr9gOsQA==", null, false, "a9c8b9df-f23d-4302-8302-90bed5c6101e", false, "Dev02" },
                    { "b1c7fa29-5a6d-4fbf-83e8-8ed0303c5133", 0, "9ce342f9-a55e-45f2-a580-772e11b5ddd6", "dev03@company.com", false, false, null, "DEV03@COMPANY.COM", "DEV03", "AQAAAAEAACcQAAAAEMmw/vGKiznnJtUxHOvPYLGjcYioISxt1S1Jv21QUySFyOtlW3IccaVc0CODzLbLYg==", null, false, "0f4944da-26e8-4332-9cc6-68c2e8767e54", false, "Dev03" },
                    { "b98746ca-a527-45d9-a3e4-090f9ae7b5c7", 0, "91612309-5ee9-4f04-9b68-95460e55c437", "dev04@company.com", false, false, null, "DEV04@COMPANY.COM", "DEV04", "AQAAAAEAACcQAAAAEG3w64BP9/4gC4k2SjuPdHcvB17UFdXywGZNpCdzB5XtQPSK5qX2R3QYYAz1BKKaUg==", null, false, "147d8765-29ad-421e-a78f-29b4a614129a", false, "Dev04" }
                });

            migrationBuilder.InsertData(
                table: "Administrator",
                columns: new[] { "Id", "FirstName", "IdentityUserId", "LastName", "Role" },
                values: new object[] { 1, "Admin", "e7b5286f-3bd1-497a-89c4-1cf5b83081d5", "Admin", 0 });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { "e7b5286f-3bd1-497a-89c4-1cf5b83081d5", "22926528-aff3-45ed-a837-eb4cd431319c" },
                    { "e12ee330-d7b0-4dfb-b2ef-604d434233f1", "62965855-2391-47f7-aff6-6741d8353f1b" },
                    { "4adfbd8d-4e69-45b2-8323-9b407bf000b3", "62965855-2391-47f7-aff6-6741d8353f1b" },
                    { "dc0a641a-8bfc-4c7e-aff2-578a139752ef", "f5041f31-ac79-4236-8b57-2d8955fa8a39" },
                    { "85179301-cebb-49c1-8753-e5e5507823fc", "f5041f31-ac79-4236-8b57-2d8955fa8a39" },
                    { "b1c7fa29-5a6d-4fbf-83e8-8ed0303c5133", "f5041f31-ac79-4236-8b57-2d8955fa8a39" },
                    { "b98746ca-a527-45d9-a3e4-090f9ae7b5c7", "f5041f31-ac79-4236-8b57-2d8955fa8a39" }
                });

            migrationBuilder.InsertData(
                table: "Developer",
                columns: new[] { "Id", "FirstName", "FullName", "IdentityUserId", "LastName", "Role", "TaskCount" },
                values: new object[,]
                {
                    { 1, "Arturo", "Anand, Arturo", "dc0a641a-8bfc-4c7e-aff2-578a139752ef", "Anand", 2, 0 },
                    { 2, "Candace", "Kapoor, Candace", "85179301-cebb-49c1-8753-e5e5507823fc", "Kapoor", 2, 0 },
                    { 3, "Roger", "Abercrombie, Roger", "b1c7fa29-5a6d-4fbf-83e8-8ed0303c5133", "Abercrombie", 2, 0 },
                    { 4, "Ken", "Norman, Ken", "b98746ca-a527-45d9-a3e4-090f9ae7b5c7", "Norman", 2, 0 }
                });

            migrationBuilder.InsertData(
                table: "ProjectManager",
                columns: new[] { "Id", "FirstName", "FullName", "IdentityUserId", "LastName", "Role" },
                values: new object[,]
                {
                    { 1, "Carson", "Alexander, Carson", "e12ee330-d7b0-4dfb-b2ef-604d434233f1", "Alexander", 1 },
                    { 2, "Meredith", "Alonso, Meredith", "4adfbd8d-4e69-45b2-8323-9b407bf000b3", "Alonso", 1 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Administrator_IdentityUserId",
                table: "Administrator",
                column: "IdentityUserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Developer_IdentityUserId",
                table: "Developer",
                column: "IdentityUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Project_ProjectManagerId",
                table: "Project",
                column: "ProjectManagerId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectManager_IdentityUserId",
                table: "ProjectManager",
                column: "IdentityUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Task_DeveloperId",
                table: "Task",
                column: "DeveloperId");

            migrationBuilder.CreateIndex(
                name: "IX_Task_ProjectId",
                table: "Task",
                column: "ProjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Administrator");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Task");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Developer");

            migrationBuilder.DropTable(
                name: "Project");

            migrationBuilder.DropTable(
                name: "ProjectManager");

            migrationBuilder.DropTable(
                name: "AspNetUsers");
        }
    }
}
