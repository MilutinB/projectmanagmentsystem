﻿using ApplicationCore.Entities;
using ApplicationCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Data
{
    public static class DbSeedModelBuilderExtension
    {
        public static void DbSeed(this ModelBuilder modelBuilder)
        {
            string adminRoleId = Guid.NewGuid().ToString();
            string pmRoleId = Guid.NewGuid().ToString();
            string devRoleId = Guid.NewGuid().ToString();

            modelBuilder.Entity<IdentityRole>().HasData
            (
                new IdentityRole
                {
                    Id = adminRoleId,
                    Name = "Admin",
                    NormalizedName = "ADMIN"
                },
                new IdentityRole
                {
                    Id = pmRoleId,
                    Name = "Pm",
                    NormalizedName = "PM"
                },
                new IdentityRole
                {
                    Id = devRoleId,
                    Name = "Dev",
                    NormalizedName = "DEV"
                }
            );

            string adminUserId = Guid.NewGuid().ToString();
            string pm1UserId = Guid.NewGuid().ToString();
            string pm2UserId = Guid.NewGuid().ToString();
            string dev1UserId = Guid.NewGuid().ToString();
            string dev2UserId = Guid.NewGuid().ToString();
            string dev3UserId = Guid.NewGuid().ToString();
            string dev4UserId = Guid.NewGuid().ToString();

            PasswordHasher<IdentityUser> hasher = new PasswordHasher<IdentityUser>();

            modelBuilder.Entity<IdentityUser>().HasData
            (
                new IdentityUser
                {
                    Id = adminUserId,
                    UserName = "Admin",
                    NormalizedUserName = "ADMIN",
                    Email = "admin@company.com",
                    NormalizedEmail = "ADMIN@COMPANY.COM",
                    ConcurrencyStamp = Guid.NewGuid().ToString(),
                    SecurityStamp = Guid.NewGuid().ToString(),
                    PasswordHash = hasher.HashPassword(null, "adminpass")
                },
                new IdentityUser
                {
                    Id = pm1UserId,
                    UserName = "Pm01",
                    NormalizedUserName = "PM01",
                    Email = "pm01@company.com",
                    NormalizedEmail = "PM01@COMPANY.COM",
                    ConcurrencyStamp = Guid.NewGuid().ToString(),
                    SecurityStamp = Guid.NewGuid().ToString(),
                    PasswordHash = hasher.HashPassword(null, "pmpass")
                },
                new IdentityUser
                {
                    Id = pm2UserId,
                    UserName = "Pm02",
                    NormalizedUserName = "PM02",
                    Email = "pm02@company.com",
                    NormalizedEmail = "PM02@COMPANY.COM",
                    ConcurrencyStamp = Guid.NewGuid().ToString(),
                    SecurityStamp = Guid.NewGuid().ToString(),
                    PasswordHash = hasher.HashPassword(null, "pmpass")
                },
                new IdentityUser
                {
                    Id = dev1UserId,
                    UserName = "Dev01",
                    NormalizedUserName = "DEV01",
                    Email = "dev01@company.com",
                    NormalizedEmail = "DEV01@COMPANY.COM",
                    ConcurrencyStamp = Guid.NewGuid().ToString(),
                    SecurityStamp = Guid.NewGuid().ToString(),
                    PasswordHash = hasher.HashPassword(null, "devpass")
                },
                new IdentityUser
                {
                    Id = dev2UserId,
                    UserName = "Dev02",
                    NormalizedUserName = "DEV02",
                    Email = "dev02@company.com",
                    NormalizedEmail = "DEV02@COMPANY.COM",
                    ConcurrencyStamp = Guid.NewGuid().ToString(),
                    SecurityStamp = Guid.NewGuid().ToString(),
                    PasswordHash = hasher.HashPassword(null, "devpass")
                },
                new IdentityUser
                {
                    Id = dev3UserId,
                    UserName = "Dev03",
                    NormalizedUserName = "DEV03",
                    Email = "dev03@company.com",
                    NormalizedEmail = "DEV03@COMPANY.COM",
                    ConcurrencyStamp = Guid.NewGuid().ToString(),
                    SecurityStamp = Guid.NewGuid().ToString(),
                    PasswordHash = hasher.HashPassword(null, "devpass")
                },
                new IdentityUser
                {
                    Id = dev4UserId,
                    UserName = "Dev04",
                    NormalizedUserName = "DEV04",
                    Email = "dev04@company.com",
                    NormalizedEmail = "DEV04@COMPANY.COM",
                    ConcurrencyStamp = Guid.NewGuid().ToString(),
                    SecurityStamp = Guid.NewGuid().ToString(),
                    PasswordHash = hasher.HashPassword(null, "devpass")
                }
            );

            modelBuilder.Entity<IdentityUserRole<string>>().HasData
            (
                new IdentityUserRole<string>
                {
                    RoleId = adminRoleId,
                    UserId = adminUserId
                },
                new IdentityUserRole<string>
                {
                    RoleId = pmRoleId,
                    UserId = pm1UserId
                },
                new IdentityUserRole<string>
                {
                    RoleId = pmRoleId,
                    UserId = pm2UserId
                },
                new IdentityUserRole<string>
                {
                    RoleId = devRoleId,
                    UserId = dev1UserId
                },
                new IdentityUserRole<string>
                {
                    RoleId = devRoleId,
                    UserId = dev2UserId
                },
                new IdentityUserRole<string>
                {
                    RoleId = devRoleId,
                    UserId = dev3UserId
                },
                new IdentityUserRole<string>
                {
                    RoleId = devRoleId,
                    UserId = dev4UserId
                }
            );

            modelBuilder.Entity<Administrator>().HasData
            (
                new Administrator
                {
                    Id = 1,
                    FirstName = "Admin",
                    LastName = "Admin",
                    IdentityUserId = adminUserId,
                    Role = Role.Administrator
                }
            );

            modelBuilder.Entity<ProjectManager>().HasData
            (
                new ProjectManager
                {
                    Id = 1,
                    FirstName = "Carson",
                    LastName = "Alexander",
                    IdentityUserId = pm1UserId,
                    FullName = "Alexander, Carson",
                    Role = Role.ProjectManager
                },
                new ProjectManager
                {
                    Id = 2,
                    FirstName = "Meredith",
                    LastName = "Alonso",
                    IdentityUserId = pm2UserId,
                    FullName = "Alonso, Meredith",
                    Role = Role.ProjectManager
                }
            );

            modelBuilder.Entity<Developer>().HasData
            (
                new Developer
                {
                    Id = 1,
                    FirstName = "Arturo",
                    LastName = "Anand",
                    IdentityUserId = dev1UserId,
                    FullName = "Anand, Arturo",
                    Role = Role.Developer
                },
                new Developer
                {
                    Id = 2,
                    FirstName = "Candace",
                    LastName = "Kapoor",
                    IdentityUserId = dev2UserId,
                    FullName = "Kapoor, Candace",
                    Role = Role.Developer
                },
                new Developer
                {
                    Id = 3,
                    FirstName = "Roger",
                    LastName = "Abercrombie",
                    IdentityUserId = dev3UserId,
                    FullName = "Abercrombie, Roger",
                    Role = Role.Developer
                },
                new Developer
                {
                    Id = 4,
                    FirstName = "Ken",
                    LastName = "Norman",
                    IdentityUserId = dev4UserId,
                    FullName = "Norman, Ken",
                    Role = Role.Developer
                }
            );
        }
    }
}
