﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Entities
{
    public class ProjectTask
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Progress { get; set; }
        public ProjectTaskStatus Status { get; set; }
        public DateTime Deadline { get; set; }
        public int ProjectId { get; set; }
        public int? DeveloperId { get; set; }

        public Project Project { get; set; }
        public Developer Developer { get; set; }
    }
}