﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Entities
{
    public class Project
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Progress { get; set; }
        public int ProjectManagerId { get; set; }

        public ProjectManager ProjectManager { get; set; }
        public ICollection<ProjectTask> Tasks { get; set; }
    }
}
