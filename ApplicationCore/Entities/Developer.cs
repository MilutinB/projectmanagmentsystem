﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Entities
{
    public class Developer
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Role Role { get; set; }
        public int TaskCount { get; set; }
        public string IdentityUserId { get; set; }
        public string FullName 
        { 
            get { return LastName + ", " + FirstName; } 
            set { }
        }
        public IdentityUser IdentityUser { get; set; }
        public ICollection<ProjectTask> Tasks { get; set; }
    }
}