﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;


namespace ApplicationCore.Services
{
    public class ProjectManagerService
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IProjectManagerRepository _projectManagerRepository;
        private readonly IdentityUserService _identityUserService;
        private readonly SignInManager<IdentityUser> _signInManager;

        public ProjectManagerService(UserManager<IdentityUser> userManager, 
                                     IProjectManagerRepository projectManagerRepository,
                                     IdentityUserService identityUserService,
                                     SignInManager<IdentityUser> signInManager)
        {
            _userManager = userManager;
            _projectManagerRepository = projectManagerRepository;
            _identityUserService = identityUserService;
            _signInManager = signInManager;
        }

        public async Task<IdentityResult> Create(string firstName, string lastName, string userName, string email, string password)
        {
            IdentityResult result = await _identityUserService.CreateNewUser(userName, email, password);

            if (result.Succeeded)
            {
                IdentityUser user = await _userManager.FindByNameAsync(userName);

                ProjectManager manager = new ProjectManager
                {
                    FirstName = firstName,
                    LastName = lastName,
                    IdentityUserId = user.Id,
                    Role = Role.ProjectManager
                };

                result = await _identityUserService.AddUserToPmRole(user);

                if (result.Succeeded)
                {
                    await _projectManagerRepository.AddAsync(manager);
                    return result;
                }
                throw new AggregateException($"Project manager adding to role failed. " +
                    $"Manager was created but couldn't be added to role." +
                    $"Please delete user '{userName}' with e-mail '{email}' and then try again.");
            }
            return result;
        }

        public async Task<IdentityResult> Edit(
            ClaimsPrincipal user, int id, string firstName, string lastName, string userName, string email, string password)
        {
            ProjectManager manager = await _projectManagerRepository.GetByIdAsync(id);
            IdentityUser userToUpdate = await _userManager.FindByIdAsync(manager?.IdentityUserId);
            IdentityUser currentUser = await _userManager.GetUserAsync(user);

            IdentityResult result = null;

            if (manager != null)
            {
                result = await _identityUserService.Edit(manager.IdentityUserId, userName, email, password);

                if (result.Succeeded)
                {
                    manager.FirstName = firstName;
                    manager.LastName = lastName;

                    await _projectManagerRepository.UpdateAsync(manager);

                    if (!(await _userManager.IsInRoleAsync(currentUser, "Admin")))
                    {
                        await _signInManager.RefreshSignInAsync(userToUpdate);
                    }
                    return result;
                }
                return result;
            }
            throw new AggregateException($"Manager '{manager.FirstName + " " + manager.LastName}' with id '{id}' not found.");
        }

        public async Task<IdentityResult> Delete(int? id)
        {
            ProjectManager manager = await _projectManagerRepository.GetByIdAsync(id);
            IdentityResult result = null;

            if (manager != null)
            {
                result = await _identityUserService.DeleteFromPmRole(manager.IdentityUserId);

                if (result.Succeeded)
                {
                    await _projectManagerRepository.DeleteAsync(manager);
                    result = await _identityUserService.DeleteFromUsers(manager.IdentityUserId);

                    if (result.Succeeded)
                    {
                        return result;
                    }
                    throw new AggregateException($"Manager '{manager.FirstName + " " + manager.LastName}' with id '{id}'" +
                        $" was deleted from Role and Repository but failed to delete from Users.");
                }
                return result;
            }
            throw new AggregateException($"Manager '{manager.FirstName + " " + manager.LastName}' with id '{id}' not found.");
        }

        public async Task<bool> UserHasPermission(ClaimsPrincipal user, int? id)
        {
            string userId = _userManager.GetUserId(user);
            ProjectManager manager = await _projectManagerRepository.GetByIdAsync(id);

            return manager.IdentityUserId == userId;
        }
    }
}
