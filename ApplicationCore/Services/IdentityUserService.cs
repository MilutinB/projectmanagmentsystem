﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Services
{
    public class IdentityUserService
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly PasswordHasher<IdentityUser> _passwordHasher = new PasswordHasher<IdentityUser>();
        private readonly SignInManager<IdentityUser> _signInManager;

        public IdentityUserService(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task<IdentityResult> CreateNewUser(string username, string email, string password)
        {
            IdentityUser user = new IdentityUser
            {
                UserName = username,
                Email = email
            };

            return await _userManager.CreateAsync(user, password);
        }

        public async Task<IdentityResult> AddUserToPmRole(IdentityUser user)
        {
            return await _userManager.AddToRoleAsync(user, "Pm");
        }

        public async Task<IdentityResult> AddUserToDevRole(IdentityUser user)
        {
            return await _userManager.AddToRoleAsync(user, "Dev");
        }

        public async Task<IdentityResult> Edit(string id, string username, string email, string password)
        {
            IdentityUser user = await _userManager.FindByIdAsync(id);

            user.UserName = username;
            user.Email = email;

            if (password != null)
            {
                user.PasswordHash = _passwordHasher.HashPassword(user, password);
            }
            return await _userManager.UpdateAsync(user);
        }

        public async Task<IdentityResult> DeleteFromPmRole(string id)
        {
            IdentityUser user = await _userManager.FindByIdAsync(id);
            return await _userManager.RemoveFromRoleAsync(user, "Pm");
        }

        public async Task<IdentityResult> DeleteFromDevRole(string id)
        {
            IdentityUser user = await _userManager.FindByIdAsync(id);
            return await _userManager.RemoveFromRoleAsync(user, "Dev");
        }

        public async Task<IdentityResult> DeleteFromUsers(string id)
        {
            IdentityUser user = await _userManager.FindByIdAsync(id);
            return await _userManager.DeleteAsync(user);
        }
    }
}
