﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationCore.Services
{
    public class TaskService
    {
        private readonly ITaskRepository _taskRepository;
        private readonly DeveloperService _developerService;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IDeveloperRepository _developerRepository;

        public TaskService(ITaskRepository taskRepository, 
                           DeveloperService developerService,
                           UserManager<IdentityUser> userManager,
                           IDeveloperRepository developerRepository)
        {
            _taskRepository = taskRepository;
            _developerService = developerService;
            _userManager = userManager;
            _developerRepository = developerRepository;
        }

        public async Task Create(string name, string description, DateTime deadline, int projectId, int? developerId)
        {
            ProjectTask projectTask = new ProjectTask
            {
                Name = name,
                Description = description,
                Deadline = deadline,
                ProjectId = projectId,
                Progress = 0
            };

            if (developerId != null)
            {
                projectTask.DeveloperId = developerId.Value;
                projectTask.Status = ProjectTaskStatus.InProgress;
                projectTask.DeveloperId = developerId.Value;
            }
            else
            {
                projectTask.Status = ProjectTaskStatus.New;
                projectTask.DeveloperId = null;
            }

            await _taskRepository.AddAsync(projectTask);
        }

        public async Task Edit(int id, string name, string description, DateTime deadline, int progress, 
            int? developerId, int projectId)
        {
            ProjectTask task = await _taskRepository.GetByIdAsync(id);

            if (task != null)
            {
                task.Name = name;
                task.Description = description;
                task.Deadline = deadline;
                task.Progress = progress;
                task.ProjectId = projectId;
            }

            if (progress == 100)
            {
                task.Status = ProjectTaskStatus.Finished;
            }
            else
            {
                task.Status = ProjectTaskStatus.InProgress;
            }

            if (developerId != null)
            {
                if (developerId != task.DeveloperId)
                {
                    await _developerService.IncreaseTaskCount(developerId.Value);

                    if (task.DeveloperId != null)
                    {
                        await _developerService.DecreaseTaskCount(task.DeveloperId.Value);
                    }
                }

                if (progress == 100)
                {
                    await _developerService.DecreaseTaskCount(developerId.Value);
                }
                task.DeveloperId = developerId;
            }
            else
            {
                await _developerService.DecreaseTaskCount(task.DeveloperId.Value);
                task.DeveloperId = null;

                if (progress != 100)
                {
                    task.Status = ProjectTaskStatus.New;
                }
            }
            await _taskRepository.UpdateAsync(task);
        }

        public async Task Delete(int id, int? developerId)
        {
            ProjectTask task = await _taskRepository.GetByIdAsync(id);

            if (task != null)
            {
                await _taskRepository.DeleteAsync(task);
            }

            if (developerId != null && task.Progress != 100)
            {
                await _developerService.DecreaseTaskCount(developerId.Value);
            }
        }

        public async Task<bool> UserHasPermission(ClaimsPrincipal user, int? id)
        {
            string userId = _userManager.GetUserId(user);
            Developer developer = (await _developerRepository.GetAsync(d => d.IdentityUserId == userId)).FirstOrDefault();
            ProjectTask task = await _taskRepository.GetByIdAsync(id);

            return task.DeveloperId == developer.Id;
        }
    }
}
