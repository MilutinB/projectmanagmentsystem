﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Services
{
    public class ProjectService
    {
        private readonly IProjectRepository _projectRepository;
        private readonly ITaskRepository _taskRepository;
        private readonly DeveloperService _developerService;
        private readonly IDeveloperRepository _developerRepository;

        public ProjectService(IProjectRepository projectRepository, 
                              ITaskRepository taskRepository,
                              DeveloperService developerService,
                              IDeveloperRepository developerRepository)
        {
            _projectRepository = projectRepository;
            _taskRepository = taskRepository;
            _developerService = developerService;
            _developerRepository = developerRepository;
        }

        public async Task Create(string name, int managerId)
        {
            Project project = new Project
            {
                Name = name,
                ProjectManagerId = managerId,
                Progress = 0,
                Code = CreateProjectCode()
            };
            await _projectRepository.AddAsync(project);
        }

        public async Task Edit(int id, string name, int managerId)
        {
            Project project = await GetProject(id);

            if (project != null)
            {
                project.Name = name;
                project.ProjectManagerId = managerId;
                await _projectRepository.UpdateAsync(project);
            }
        }

        public async Task Delete(int id)
        {
            IReadOnlyCollection<ProjectTask> tasks = await _taskRepository.GetAsync(t => t.ProjectId == id, t => t.Developer);

            if (tasks != null)
            {
                foreach (ProjectTask task in tasks)
                {
                    await _taskRepository.DeleteAsync(task);

                    if (task.DeveloperId != null)
                    {
                        Developer developer = await _developerRepository.GetByIdAsync(task.DeveloperId);

                        if (!(developer.TaskCount < 1) && task.Progress != 100)
                        {
                            await _developerService.DecreaseTaskCount(task.DeveloperId.Value);
                        }
                    }
                }
            }

            Project project = await GetProject(id);
            await _projectRepository.DeleteAsync(project);
        }

        public async Task<Project> GetProject(int id)
        {
            return (await _projectRepository.GetAsync(p => p.Id == id, p => p.ProjectManager, p => p.Tasks)).FirstOrDefault();
        }
        
        public async Task<Project> GetProject(int id, ProjectTaskStatus taskStatus)
        {
            return (await _projectRepository.GetAsync(p => p.Id == id, p => p.ProjectManager, 
                p => p.Tasks.Where(t => t.Status == taskStatus))).FirstOrDefault();
        }

        private string CreateProjectCode()
        {
            string guid = Guid.NewGuid().ToString();
            string[] splitGuid = guid.Split('-', StringSplitOptions.RemoveEmptyEntries);
            return splitGuid.Aggregate((current, next) => current + next);
        }
    }
}
