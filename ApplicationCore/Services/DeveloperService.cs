﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace ApplicationCore.Services
{
    public class DeveloperService
    {
        private readonly IDeveloperRepository _developerRepository;
        private readonly IdentityUserService _identityUserService;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly ITaskRepository _taskRepository;

        public DeveloperService(IDeveloperRepository developerRepository,
                                IdentityUserService identityUserService,
                                UserManager<IdentityUser> userManager,
                                SignInManager<IdentityUser> signInManager,
                                ITaskRepository taskRepository)
        {
            _developerRepository = developerRepository;
            _identityUserService = identityUserService;
            _userManager = userManager;
            _signInManager = signInManager;
            _taskRepository = taskRepository;
        }

        public async Task<IdentityResult> Create(string firstName, string lastName, string userName, string email, string password)
        {
            IdentityResult result = await _identityUserService.CreateNewUser(userName, email, password);

            if (result.Succeeded)
            {
                IdentityUser user = await _userManager.FindByNameAsync(userName);

                Developer developer = new Developer
                {
                    FirstName = firstName,
                    LastName = lastName,
                    IdentityUserId = user.Id,
                    Role = Role.Developer
                };

                result = await _identityUserService.AddUserToDevRole(user);

                if (result.Succeeded)
                {
                    await _developerRepository.AddAsync(developer);
                    return result;
                }
                throw new AggregateException($"Developer adding to role failed. " +
                    $"Developer was created but couldn't be added to role." +
                    $"Please delete user '{userName}' with e-mail '{email}' and then try again.");
            }
            return result;
        }

        public async Task<IdentityResult> Edit(
            ClaimsPrincipal user, int id, string firstName, string lastName, string userName, string email, string password)
        {
            Developer developer = await _developerRepository.GetByIdAsync(id);
            IdentityUser userToUpdate = await _userManager.FindByIdAsync(developer?.IdentityUserId);
            IdentityUser currentUser = await _userManager.GetUserAsync(user);

            IdentityResult result = null;

            if (developer != null)
            {
                result = await _identityUserService.Edit(developer.IdentityUserId, userName, email, password);

                if (result.Succeeded)
                {
                    developer.FirstName = firstName;
                    developer.LastName = lastName;

                    await _developerRepository.UpdateAsync(developer);

                    if (!(await _userManager.IsInRoleAsync(currentUser, "Admin")))
                    {
                        await _signInManager.RefreshSignInAsync(userToUpdate);
                    }
                    return result;
                }
                return result;
            }
            throw new AggregateException($"Developer '{developer.FirstName + " " + developer.LastName}' with id '{id}' not found.");
        }

        public async Task<IdentityResult> Delete(int? id)
        {
            Developer developer = (await _developerRepository.GetAsync(d => d.Id == id, d => d.Tasks)).FirstOrDefault();
            IdentityResult result = null;

            if (developer != null)
            {
                result = await _identityUserService.DeleteFromDevRole(developer.IdentityUserId);

                if (result.Succeeded)
                {
                    if (developer.Tasks != null)
                    {
                        await RemoveDeveloperFromTasks(developer.Tasks);
                    }
                    await _developerRepository.DeleteAsync(developer);
                    result = await _identityUserService.DeleteFromUsers(developer.IdentityUserId);

                    if (result.Succeeded)
                    {
                        return result;
                    }
                    throw new AggregateException($"Manager '{developer.FirstName + " " + developer.LastName}' with id '{id}'" +
                        $" was deleted from Role and Repository but failed to delete from Users.");
                }
                return result;
            }
            throw new AggregateException($"Manager '{developer.FirstName + " " + developer.LastName}' with id '{id}' not found.");
        }

        private async Task RemoveDeveloperFromTasks(ICollection<ProjectTask> tasks)
        {
            foreach (ProjectTask task in tasks.ToList())
            {
                task.DeveloperId = null;

                if (task.Progress != 100)
                {
                    task.Status = ProjectTaskStatus.New;
                }
                
                await _taskRepository.UpdateAsync(task);
            }
        }

        public async Task IncreaseTaskCount(int id)
        {
            Developer developer = await _developerRepository.GetByIdAsync(id);

            if (developer != null)
            {
                developer.TaskCount++;
                await _developerRepository.UpdateAsync(developer);
            }
        } 

        public async Task DecreaseTaskCount(int id)
        {
            Developer developer = await _developerRepository.GetByIdAsync(id);

            if (developer != null)
            {
                developer.TaskCount--;
                await _developerRepository.UpdateAsync(developer);
            }
        }

        public async Task<bool> UserHasPermission(ClaimsPrincipal user, int? id)
        {
            string userId = _userManager.GetUserId(user);
            Developer developer = await _developerRepository.GetByIdAsync(id);

            return developer.IdentityUserId == userId;
        }
    }
}
