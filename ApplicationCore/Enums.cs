﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore
{
    public enum ProjectTaskStatus
    {
        New,
        InProgress,
        Finished
    }

    public enum Role
    {
        Administrator,
        ProjectManager,
        Developer
    }
}
